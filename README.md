# University of Bonn: Pattern Recognition SS2019
Code from the Pattern Recognition project at University of Bonn (SS19)

## Welcome
Welcome one and all. Please follow the tiny set of instruction to keep track of what is happening in the project.

1. Please pull the code in your local repos whenever you start working so that you get the latest version of the code.
2. Whenever you contribute to anything, please update the kanban project description. If you want to overwrite something just use the "~~strikethrough~~" instead of just deleting the previous requirement. Thanks.
3. Whenever you think that there are any tremendous issues in the code just raise an "Issue" on the repository.

Thanks for reading. For anything else, you already know the Whatsapp group.

PS: All of this might look excessive but its better than keeping no track of anything. :P
